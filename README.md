Tool for encrypting, compressing, and uploading zfs send streams to S3 or other compatible
object stores. Uses zstd for compression and gpg asymmetric keys for encryption+signing.

The `zfs send` and `zfs receive` features are cool and all, if your backup service is also running zstd and
you have access to it. At the time of this writing, the only third party service that offers this is `rsync.net`,
and their price of 2.5 cents USD per gigabyte is simply not compeditive with S3's 0.023 cents per gigabyte (with
infrequent access tiers and other vendors being cheaper).

This tool implements sending and receiving `zstd send` streams to S3 or other object stores, and provides some
basic utilities for managing those backups. Streams are compressed with ZSTD, and signed+encrypted with GPG on
the client side.

Dependencies
============

You will need the `zstd` command line program as well as `gpg`.

Setup
=====

Backup
------

On the backup machine, as root, create a GPG signing key, to sign backups with, and import the
public key of the GPG key you wish to encrypt backups with. Export and store the singing public key,
to verify backups later.

Set up a config file for zfs3bk, using `example-cfg.toml` as a base. Be sure to give it 0600 permissions,
as it contains your AWS secret token.

After that, run `zfs3bk /path/to/config.toml upload <args>` to upload snapshots.

Restore
-------

Import the private key used to encrypt the backups with, and the public signing key.

Restore the config file, so that zfs3bk can access the bucket where they are stored.

After that, run `zfs3bk /path/to/config.toml download <args>` to download a snapshot.

Automatic Backups
-----------------

Fill in the `[auto]` section of the configuration file, then run `zfs3bk /path/to/cfg.toml automatic-snapshot` on a
systemd timer or cron job however frequently you want to take a backup.

If you want more flexibility, consider using a program like `zrepl` and running zfs3bk as a post-snapshot hook.

GPG and sudo
============

GPG and the tty layer don't like it when the user that GPG is executing as doesn't match
the real user. However, zfs3bk needs to run as admin to read and write the zfs streams.

To help alleviate this, the `gpg_upload_user` and `gpg_download_user` options can be set in the config
to run GPG as a different user, ie your login user. This should alleviate GPG-related errors, but
can be a security risk, as the passed-in user will have access to the stream should they monitor the
GPG process.

zfs3bk will also set the `GPG_TTY` environment variable, if not set already, to the output of the `tty`
program (if it does not fail), help with entering passwords and PINs.

Manual Restore
==============

You can extract the backup streams without zfs3bk if you need to:

	# >/path/to/backup gpg --decrypt | unzstd | zfs receive dest/filesystem@snapshot

Logging
=======

This project uses the env_logger crate for logging. To alter logging, set the `RUST_LOG` environmental variable
to a level, such as `info` or `debug`.
