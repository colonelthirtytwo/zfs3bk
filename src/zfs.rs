//! Crappy interface to zfs using subprocesses
//!
//! TODO: Use a real library, once one exists and is stable.

use std::collections::HashMap;
use std::process;

/// Lists all snapshots for a ZFS filesystem.
///
/// The returned list elements contain just the name of the snapshot, i.e. they are
/// not prefixed with the filesystem name or a `@` character.
pub async fn list_fs_snapshots(filesystem: &str) -> Result<Vec<String>, String> {
	let out = tokio::process::Command::new("zfs")
		.arg("list")
		.arg("-H")
		.arg("-t")
		.arg("snapshot")
		.arg("-o")
		.arg("name")
		.arg("--")
		.arg(filesystem)
		.stdin(process::Stdio::null())
		.stdout(process::Stdio::piped())
		.stderr(process::Stdio::inherit())
		.output()
		.await
		.map_err(|e| format!("Could not execute zfs; is it installed? {}", e))?;
	if !out.status.success() {
		return Err(format!("zfs list failed with code {}", out.status));
	}

	Ok(String::from_utf8_lossy(&out.stdout)
		.trim()
		.split('\n')
		.map(str::trim)
		.filter_map(|s| {
			if s.starts_with(filesystem) && s[filesystem.len()..].starts_with('@') {
				Some(&s[filesystem.len() + 1..])
			} else {
				None
			}
		})
		.map(String::from)
		.collect())
}

pub async fn list_all_snapshots() -> Result<Vec<(String, String)>, String> {
	let out = tokio::process::Command::new("zfs")
		.arg("list")
		.arg("-H")
		.arg("-t")
		.arg("snapshot")
		.arg("-o")
		.arg("name")
		.arg("--")
		.stdin(process::Stdio::null())
		.stdout(process::Stdio::piped())
		.stderr(process::Stdio::inherit())
		.output()
		.await
		.map_err(|e| format!("Could not execute zfs; is it installed? {}", e))?;
	if !out.status.success() {
		return Err(format!("zfs list failed with code {}", out.status));
	}

	Ok(String::from_utf8_lossy(&out.stdout)
		.trim()
		.split('\n')
		.map(str::trim)
		.filter_map(|s| s.split_once('@'))
		.map(|(fs, name)| (fs.to_owned(), name.to_owned()))
		.collect())
}

pub async fn create_snapshot(
	filesystem: &str,
	snapshot_name: &str,
	properties: &HashMap<String, String>,
) -> Result<(), String> {
	if snapshot_name.contains('@') {
		return Err("Snapshot name may not contain '@' character".into());
	}
	let mut cmd = tokio::process::Command::new("zfs");
	cmd.arg("snapshot");
	for (k, v) in properties.iter() {
		if k.contains('=') {
			return Err(format!("Key {:?} cannot contain '=' character", k));
		}
		cmd.arg("-o").arg(format!("{}={}", k, v));
	}
	let res = cmd
		.arg("--")
		.arg(format!("{}@{}", filesystem, snapshot_name))
		.stdin(process::Stdio::null())
		.stdout(process::Stdio::inherit())
		.stderr(process::Stdio::inherit())
		.status()
		.await
		.map_err(|e| format!("Could not execute zfs; is it installed? {}", e))?;

	if !res.success() {
		return Err(format!("zfs snapshot failed with code {}", res));
	}
	Ok(())
}

pub async fn remove_snapshot(filesystem: &str, snapshot_name: &str) -> Result<(), String> {
	if snapshot_name.contains('@') {
		return Err("Snapshot name may not contain '@' character".into());
	}
	let res = tokio::process::Command::new("zfs")
		.arg("destroy")
		.arg("--")
		.arg(format!("{}@{}", filesystem, snapshot_name))
		.stdin(process::Stdio::null())
		.stdout(process::Stdio::inherit())
		.stderr(process::Stdio::inherit())
		.status()
		.await
		.map_err(|e| format!("Could not execute zfs; is it installed? {}", e))?;

	if !res.success() {
		return Err(format!("zfs snapshot failed with code {}", res));
	}
	Ok(())
}
