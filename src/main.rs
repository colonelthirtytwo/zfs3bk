//! Tool for backing up ZFS snapshots to an S3-compatible service.
//!
//! Backups are compressed, signed, and encrypted on the client side.

#[macro_use]
extern crate log;

mod auto;
mod naming;
mod procutil;
mod store;
mod transfer;
mod zfs;

use std::path::Path;
use std::path::PathBuf;
use std::sync::Arc;

use naming::RemoteBackupEntry;
use naming::RemoteBackupType;
use serde::Deserialize;
use structopt::StructOpt;

use crate::transfer::RestorePathCache;

#[derive(StructOpt)]
pub struct Args {
	/// Path to configuration
	#[structopt(parse(from_os_str))]
	pub config_path: PathBuf,
	/// Command to run
	#[structopt(subcommand)]
	pub cmd: Command,
}

#[derive(StructOpt)]
pub enum Command {
	/// List backups on the store server
	ListRemoteBackups {
		/// If provided, list only backups for this filesystem
		filesystem: Option<String>,

		/// If provided, view backups for this host instead of the current one
		#[structopt(short = "h", long, conflicts_with = "all-hosts")]
		hostname: Option<String>,

		/// If provided, view backups for all hosts on the store
		#[structopt(short = "H", long, conflicts_with = "hostname")]
		all_hosts: bool,

		/// Scripting mode. Print results in pipe-separated value format. Don't exit with a
		/// failure status code for an empty result set.
		#[structopt(short = "p", long)]
		psv: bool,
	},
	/// Upload a snapshot to a backup
	Upload {
		/// Filesystem of snapshot to upload
		filesystem: String,
		/// First snapshot. If neither a second snapshot nor -A/--auto-from are specified,
		/// does a full backup with this snapshot. If -A is specified, this is the target snapshot.
		/// Otherwise this is the base snapshot for an incremental backup.
		snapshot_1: String,
		/// Second snapshot. If specified, performs an incremental backup from the previously specified
		/// snapshot.
		snapshot_2: Option<String>,

		/// Automatic incremental snapshot mode. If specified, only one snapshot should be specified in the positional arguments.
		/// Automatically finds a "from" snapshot, by picking the latest snapshot that is available both locally and remote.
		/// If no suitable snapshot is found, does a full backup.
		#[structopt(short = "A", long, conflicts_with = "snapshot_to")]
		auto_from: bool,
	},
	/// Download and restore (or check) a backup to a snapshot
	Download {
		/// Filesystem of snapshot to download
		filesystem: String,
		/// First snapshot. If this is the only snapshot specified, attempts to fetch the matching full backup.
		snapshot_from: String,
		/// First snapshot. If this is specified, attempts to fetch the matching incremental backup.
		snapshot_to: Option<String>,
		/// Destination. For full backups, must be a name of a filesystem to create. For incremental backups, must be
		/// the full name (i.e. with filesystem and @ prefix) of a snapshot to create.
		/// By default, figures out the destination based on the backup name.
		#[structopt(short = "D", long)]
		destination: Option<String>,
		/// Passes `-n` to `zfs receive`. Download, uncompress, and check, but don't actually restore.
		/// Note that this will also check if the destination can be recovered to (ex. filesystem exists, snapshot doesn't,
		/// no changes).
		#[structopt(short = "n", long)]
		dry_run: bool,
		/// Passes `-F` to `zfs receive`. Forces rollback to most recent snapshot before receiving.
		#[structopt(short = "F", long)]
		rollback: bool,
	},
	/// Finds the series of remote backups that would be needed to be restored to restore to a snapshot.
	RestorePath {
		filesystem: String,
		snapshot: String,
		/// Scripting mode. Print results in pipe-separated value format.
		#[structopt(short = "p", long)]
		psv: bool,
		/// If provided, view backups for this host instead of the current one
		#[structopt(short = "h", long)]
		hostname: Option<String>,
	},
	/// Takes a snapshot and uploads a backup, according to the `auto` section.
	AutomaticSnapshot {},
}

#[derive(Debug, Deserialize)]
pub struct Config {
	/// Template string to name objects on the remote server.
	///
	/// It must contain each of these substrings exactly once: `{hostname}`, `{filesystem}` and `{snapshot}`.
	/// Those substrings will be replaced with the corresponding values when creating a backup, or parsed
	/// from object names when listing backups.
	///
	/// The order of the placeholders does not matter, but for efficient prefix lookups, it's recommended
	/// to specify them in the order `{hostname}`, `{filesystem}`, then `{snapshot}`.
	pub backup_name_spec: naming::BackupNameSpec,
	/// S3 (or other compatible store) configuration
	pub store: store::StoreConf,

	/// Computer's hostname. Defaults to getting it automatically, but you can overwrite it here.
	/// Note that this affects backup paths!
	#[serde(default = "default_hostname")]
	pub hostname: String,
	/// GPG key to sign backups with. Corresponds to the `--local-user` argument to GPG. If not specified,
	/// GPG will pick a default.
	pub gpg_sender_key: Option<String>,
	/// GPG key to encrypt with. Corresponds to the `--recipient` argument to GPG. Make sure the gpg user has this
	/// key in their keyring.
	pub gpg_receiver_key: String,
	/// User to run GPG as when encrypting backups. Defaults to root, which may cause issues.
	#[serde(default)]
	pub gpg_upload_user: Option<String>,
	/// User to run GPG as when decrypting backups. Defaults to root, which may cause issues.
	#[serde(default)]
	pub gpg_download_user: Option<String>,
	/// ZSTD compression level. Defaults to 3.
	#[serde(default)]
	pub zstd_compression_level: i32,
	/// Storage class name to upload backups as.
	#[serde(default)]
	pub storage_class: Option<String>,

	#[serde(default)]
	pub auto: Option<auto::AutoConfig>,
}
impl Config {
	pub fn read(path: &Path) -> Result<Self, String> {
		let f = std::fs::read(path).map_err(|e| format!("Could not read config file: {}", e))?;
		let c =
			toml::de::from_slice(&f).map_err(|e| format!("Could not parse config file: {}", e))?;
		Ok(c)
	}
}

fn default_hostname() -> String {
	hostname::get()
		.expect("Could not get hostname")
		.into_string()
		.expect("hostname was not valid utf-8")
}

#[tokio::main]
async fn main() {
	if let Err(e) = real_main().await {
		eprintln!("{}", e);
		std::process::exit(1);
	}
}

async fn real_main() -> Result<(), String> {
	let args = Args::from_args();

	env_logger::init();

	let config = Config::read(&args.config_path)?;

	match args.cmd {
		Command::ListRemoteBackups {
			hostname,
			filesystem,
			all_hosts,
			psv,
		} => {
			let hostname = if all_hosts {
				None
			} else if let Some(ref hostname) = hostname {
				Some(hostname.as_str())
			} else {
				Some(config.hostname.as_str())
			};

			let store = config.store.create_client()?;

			let local_snapshots = if hostname == Some(config.hostname.as_str()) {
				zfs::list_all_snapshots().await?
			} else {
				vec![]
			};

			let mut entries = crate::transfer::list_remotes(
				&store,
				&config,
				hostname.as_deref(),
				filesystem.as_deref(),
			)
			.await?;
			if entries.is_empty() && !psv {
				return Err("No results".into());
			}
			entries.sort_unstable();
			let restore_path_cache = RestorePathCache::new(&entries);

			for entry in entries.iter() {
				if psv {
					print!("{}|{}|", entry.hostname, entry.filesystem);
					match &entry.typ {
						naming::RemoteBackupType::Full(name) => print!("{}||", name),
						naming::RemoteBackupType::Partial { start, end } => {
							print!("{}|{}|", start, end)
						}
					};

					if entry.hostname != config.hostname {
						print!("foreign")
					} else if local_snapshots
						.iter()
						.any(|(fs, name)| fs == &entry.filesystem && name == entry.typ.end())
					{
						print!("available")
					} else if restore_path_cache
						.path_to(&entry.hostname, &entry.filesystem, entry.typ.end())
						.is_some()
					{
						print!("restorable")
					} else {
						print!("orphan")
					}
					println!();
				} else {
					match &entry.typ {
						naming::RemoteBackupType::Full(_) => print!("Full: "),
						naming::RemoteBackupType::Partial { .. } => print!("Incr: "),
					};
					if hostname.is_none() {
						print!("Host: {:?} ", entry.hostname);
					}
					if filesystem.is_none() {
						print!("FS: {:?} ", entry.filesystem);
					}

					match &entry.typ {
						naming::RemoteBackupType::Full(name) => print!("Snapshot {:?} ", name),
						naming::RemoteBackupType::Partial { start, end } => {
							print!("Snapshot {:?} to {:?} ", start, end)
						}
					};

					if entry.hostname != config.hostname {
					} else if local_snapshots
						.iter()
						.any(|(fs, name)| fs == &entry.filesystem && name == entry.typ.end())
					{
						print!("(locally available)")
					} else if restore_path_cache
						.path_to(&entry.hostname, &entry.filesystem, entry.typ.end())
						.is_some()
					{
					} else {
						print!("(Orphan, unrestorable!!!)")
					}
					println!();
				}
			}
		}
		Command::Upload {
			filesystem,
			snapshot_1,
			snapshot_2,
			auto_from,
		} => {
			if !nix::unistd::geteuid().is_root() {
				return Err("Refusing to run as non-root (zfs send won't work).".into());
			}
			let store = config.store.create_client()?;
			let snlist = zfs::list_fs_snapshots(&filesystem).await?;

			if !snlist.contains(&snapshot_1) {
				return Err(format!(
					"Snapshot {:?} on filesystem {:?} does not exist",
					snapshot_1, filesystem
				));
			}

			let dest_entry_typ = match (auto_from, snapshot_2) {
				(true, _) => {
					let remote_backups = crate::transfer::list_remotes(
						&store,
						&config,
						Some(&config.hostname),
						Some(&filesystem),
					)
					.await?;

					let to_pos = snlist.iter().position(|s| *s == snapshot_1).unwrap();
					if let Some(from) = snlist[0..to_pos]
						.iter()
						.rev()
						.find(|sn| remote_backups.iter().any(|e| e.typ.end() == *sn))
					{
						info!("-A/--auto-from: Using snapshot {:?} as base", from);
						RemoteBackupType::Partial {
							start: from.clone(),
							end: snapshot_1,
						}
					} else {
						info!("-A/--auto-from: Did not find matching snapshot, doing full backup");
						RemoteBackupType::Full(snapshot_1)
					}
				}
				(false, Some(to)) => {
					if !snlist.contains(&to) {
						return Err(format!(
							"Snapshot {:?} on filesystem {:?} does not exist",
							to, filesystem
						));
					}
					RemoteBackupType::Partial {
						start: snapshot_1,
						end: to,
					}
				}
				(false, None) => RemoteBackupType::Full(snapshot_1),
			};

			let dest_entry = RemoteBackupEntry {
				hostname: config.hostname.clone(),
				filesystem,
				typ: dest_entry_typ,
			};

			let dest_name = dest_entry.format_with(&config.backup_name_spec);
			if store.object_exists(&dest_name).await? {
				return Err(format!("Key {:?} already exists", dest_name));
			}

			crate::transfer::upload(&config, Arc::new(store), dest_entry).await?;
		}
		Command::Download {
			filesystem,
			snapshot_from,
			snapshot_to,
			destination,
			dry_run,
			rollback,
		} => {
			if !nix::unistd::geteuid().is_root() {
				return Err("Refusing to run as non-root (zfs receive won't work).".into());
			}

			let store = config.store.create_client()?;

			let dest_entry = RemoteBackupEntry {
				hostname: config.hostname.clone(),
				filesystem,
				typ: match snapshot_to {
					Some(snapshot_to) => RemoteBackupType::Partial {
						start: snapshot_from,
						end: snapshot_to,
					},
					None => RemoteBackupType::Full(snapshot_from),
				},
			};

			let dest_name = dest_entry.format_with(&config.backup_name_spec);
			if !store.object_exists(&dest_name).await? {
				return Err(format!("Key {:?} does not exist", dest_name));
			}

			crate::transfer::download(
				&config,
				Arc::new(store),
				dest_entry,
				destination.as_deref(),
				dry_run,
				rollback,
			)
			.await?;
		}
		Command::RestorePath {
			filesystem,
			snapshot,
			psv,
			hostname,
		} => {
			let hostname = hostname.as_deref().unwrap_or(config.hostname.as_str());
			let store = config.store.create_client()?;
			let entries =
				crate::transfer::list_remotes(&store, &config, Some(hostname), Some(&filesystem))
					.await?;
			let path = RestorePathCache::new(&entries).path_to(hostname, &filesystem, &snapshot);
			if let Some(path) = path {
				for entry in path.iter() {
					if psv {
						print!("{}|{}|", entry.hostname, entry.filesystem);
						match &entry.typ {
							naming::RemoteBackupType::Full(name) => print!("{}|", name),
							naming::RemoteBackupType::Partial { start, end } => {
								print!("{}|{}", start, end)
							}
						};
						println!();
					} else {
						match &entry.typ {
							naming::RemoteBackupType::Full(_) => print!("Full: "),
							naming::RemoteBackupType::Partial { .. } => print!("Incr: "),
						};

						match &entry.typ {
							naming::RemoteBackupType::Full(name) => println!("Snapshot {:?}", name),
							naming::RemoteBackupType::Partial { start, end } => {
								println!("Snapshot {:?} to {:?}", start, end)
							}
						};
					}
				}
			} else {
				return Err(format!(
					"No path to snapshot {:?}:{:?}@{:?}",
					hostname, filesystem, snapshot
				));
			}
		}
		Command::AutomaticSnapshot {} => {
			let auto = match config.auto.as_ref() {
				Some(v) => v,
				None => return Err("auto section missing from config file".into()),
			};
			let store = config.store.create_client()?;

			auto::auto_snapshot(Arc::new(store), &config, auto).await?;
		}
	}
	Ok(())
}
