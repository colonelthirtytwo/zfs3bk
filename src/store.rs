use std::str::FromStr;

use rusoto_s3::AbortMultipartUploadRequest;
use rusoto_s3::CompleteMultipartUploadRequest;
use rusoto_s3::CompletedMultipartUpload;
use rusoto_s3::CompletedPart;
use rusoto_s3::CreateMultipartUploadRequest;
use rusoto_s3::GetObjectRequest;
use rusoto_s3::HeadObjectError;
use rusoto_s3::HeadObjectRequest;
use rusoto_s3::ListObjectsV2Request;
use rusoto_s3::UploadPartRequest;
use rusoto_s3::S3;
use serde::Deserialize;
use tokio::io::AsyncReadExt;
use tokio::io::AsyncWriteExt;

/// S3 (or compatible) data store configuration
#[derive(Debug, Deserialize)]
pub struct StoreConf {
	/// AWS region, or custom endpoint
	pub region: String,
	/// Bucket to access backups in
	pub bucket: String,
	/// Access key, for auth
	pub access_key: String,
	/// Secret access key, for auth
	pub secret_access_key: String,

	/// Size, in bytes, to read/write at a time from/to the store.
	/// Larger values require more RAM. Default is 256MB. Must be at least 5MB or S3 will reject it.
	#[serde(default = "default_stream_chunk_size")]
	pub stream_chunk_size: usize,
}

fn default_stream_chunk_size() -> usize {
	256 * 1024 * 1024
}

impl StoreConf {
	pub fn create_client(&self) -> Result<Store, String> {
		let creds = rusoto_credential::StaticProvider::new_minimal(
			self.access_key.clone(),
			self.secret_access_key.clone(),
		);
		let region =
			rusoto_signature::region::Region::from_str(&self.region).unwrap_or_else(|_| {
				rusoto_signature::region::Region::Custom {
					name: self.region.clone(),
					endpoint: self.region.clone(),
				}
			});
		let dispatcher = rusoto_core::request::HttpClient::new()
			.map_err(|e| format!("Could not create S3 HTTP client: {}", e))?;
		let client = rusoto_s3::S3Client::new_with(dispatcher, creds, region);
		Ok(Store {
			client,
			bucket: self.bucket.clone(),
			stream_chunk_size: self.stream_chunk_size,
		})
	}
}

pub struct Store {
	pub client: rusoto_s3::S3Client,
	pub bucket: String,
	pub stream_chunk_size: usize,
}
impl Store {
	pub async fn list_all_objects(&self, prefix: Option<&str>) -> Result<Vec<String>, String> {
		let mut continuation = None;
		let mut list = vec![];
		loop {
			let res = self
				.client
				.list_objects_v2(ListObjectsV2Request {
					bucket: self.bucket.clone(),
					prefix: prefix.map(String::from),
					continuation_token: continuation.take(),
					..Default::default()
				})
				.await
				.map_err(|e| format!("Could not list bucket objects: {}", e))?;

			list.extend(
				res.contents
					.unwrap_or_default()
					.into_iter()
					.map(|v| v.key.unwrap()),
			);

			if res.is_truncated.unwrap_or_default() {
				continuation = Some(res.continuation_token.unwrap());
			} else {
				break;
			}
		}
		Ok(list)
	}

	pub async fn object_exists(&self, key: &str) -> Result<bool, String> {
		match self
			.client
			.head_object(HeadObjectRequest {
				bucket: self.bucket.clone(),
				key: key.into(),
				..Default::default()
			})
			.await
		{
			Ok(_) => Ok(true),
			Err(rusoto_core::RusotoError::Service(HeadObjectError::NoSuchKey(_))) => Ok(false),
			Err(rusoto_core::RusotoError::Unknown(resp)) if resp.status.as_u16() == 404 => {
				Ok(false)
			}
			Err(e) => Err(format!("Could not get object: {:?}", e)),
		}
	}

	// TODO: RACE CONDITION: if previous process failed, might read that its closed before cancel and try to "finish"
	// the bad backup.
	pub async fn upload<R: tokio::io::AsyncRead>(
		&self,
		key: &str,
		storage_class: Option<&str>,
		cancel: tokio::sync::oneshot::Receiver<()>,
		data: R,
	) -> Result<(), String> {
		let create_res = retry(5, "Creating multi part upload", || {
			self.client
				.create_multipart_upload(CreateMultipartUploadRequest {
					bucket: self.bucket.clone(),
					key: key.into(),
					content_type: Some("application/octet-stream".into()),
					storage_class: storage_class.map(String::from),
					..Default::default()
				})
		})
		.await
		.map_err(|e| format!("Could not start multi-part upload: {}", e))?;

		let upload_id = create_res.upload_id.unwrap();

		let upload_res = self.upload_loop(&upload_id, key, cancel, data).await;

		match upload_res {
			Ok(()) => Ok(()),
			Err(e) => {
				warn!("Multi-part upload failed. Aborting it...");
				retry(5, "Aborting multi-part upload", || {
					self.client
						.abort_multipart_upload(AbortMultipartUploadRequest {
							bucket: self.bucket.clone(),
							key: key.into(),
							upload_id: upload_id.clone(),
							..Default::default()
						})
				})
				.await
				.map(|_| ())
				.unwrap_or_else(|e| {
					warn!("Could not abort multi-part upload, ignoring. Error: {}", e);
				});
				Err(e)
			}
		}
	}

	async fn upload_loop<R: tokio::io::AsyncRead>(
		&self,
		upload_id: &str,
		key: &str,
		mut cancel: tokio::sync::oneshot::Receiver<()>,
		data: R,
	) -> Result<(), String> {
		tokio::pin!(data);
		let mut parts = Vec::new();

		let mut buf = Vec::with_capacity(self.stream_chunk_size);
		'main: loop {
			// Read until full
			buf.clear();
			while buf.capacity() - buf.len() != 0 {
				let num = tokio::select! {
					res = data.read_buf(&mut buf) => res.map_err(|e| format!("Could not read: {}", e))?,
					_ = &mut cancel => return Err("S3 Upload Canceled".into()),
				};
				if num == 0 {
					// EOF
					if buf.len() == 0 {
						// Completely done
						break 'main;
					} else {
						// Send last chunk
						break;
					}
				}
			}

			debug!("Sending part {}", parts.len());

			// Send
			// TODO: need to clone the buffer each time because rusoto ByteStream is bad.
			let part_fut = retry(5, "Uploading part", || {
				self.client.upload_part(UploadPartRequest {
					bucket: self.bucket.clone(),
					key: key.into(),
					upload_id: upload_id.into(),
					part_number: parts.len() as i64 + 1,
					body: Some(buf.clone().into()),
					..Default::default()
				})
			});
			let part_response = tokio::select! {
				res = part_fut => res.map_err(|e| format!("Could not upload part {}: {}", parts.len(), e))?,
				_ = &mut cancel => return Err("S3 Upload Canceled".into()),
			};

			parts.push(CompletedPart {
				e_tag: part_response.e_tag,
				part_number: Some(parts.len() as i64 + 1),
			});
		}

		// Finish
		debug!("Completing multi-part upload");
		retry(5, "Completing multi-part upload", || {
			self.client
				.complete_multipart_upload(CompleteMultipartUploadRequest {
					bucket: self.bucket.clone(),
					key: key.into(),
					upload_id: upload_id.into(),
					multipart_upload: Some(CompletedMultipartUpload {
						parts: Some(parts.clone()),
					}),
					..Default::default()
				})
		})
		.await
		.map_err(|e| format!("Could not complete multi-part upload: {}", e))?;
		info!("S3 upload complete");
		Ok(())
	}

	pub async fn download<W: tokio::io::AsyncWrite>(
		&self,
		key: &str,
		cancel: tokio::sync::oneshot::Receiver<()>,
		dest: W,
	) -> Result<(), String> {
		tokio::pin!(dest);
		tokio::pin!(cancel);
		let res = retry(5, "s3 download", || {
			self.client.get_object(GetObjectRequest {
				bucket: self.bucket.clone(),
				key: key.into(),
				..Default::default()
			})
		})
		.await
		.map_err(|e| format!("Get object failed: {}", e))?;

		let mut offset = 0;
		let mut stream = res.body.unwrap().into_async_read();
		let mut buffer = Vec::with_capacity(self.stream_chunk_size);

		loop {
			buffer.clear();

			let res = tokio::select! {
				v = stream.read_buf(&mut buffer) => v,
				_ = &mut cancel => return Err("Canceled".into()),
			};

			match res {
				Ok(0) => {
					break;
				}
				Ok(_) => {
					dest.write_all(&buffer)
						.await
						.map_err(|e| format!("Could not write to output pipe: {}", e))?;
					offset += buffer.len();
				}
				Err(e) => {
					warn!("S3 stream read failed, refetching. Error: {}", e);
					let res = retry(5, "s3 download", || {
						self.client.get_object(GetObjectRequest {
							bucket: self.bucket.clone(),
							key: key.into(),
							range: Some(format!("bytes={}-", offset)),
							..Default::default()
						})
					})
					.await
					.map_err(|e| format!("Get object failed: {}", e))?;
					stream = res.body.unwrap().into_async_read();
				}
			}
		}
		info!("S3 Download complete");
		Ok(())
	}
}

async fn retry<
	OK,
	ERR: std::fmt::Display,
	FU: std::future::Future<Output = Result<OK, ERR>>,
	F: FnMut() -> FU,
>(
	mut count: usize,
	name: &str,
	mut func: F,
) -> Result<OK, ERR> {
	loop {
		let fut = (func)();
		tokio::pin!(fut);
		match fut.await {
			Ok(v) => return Ok(v),
			Err(e) if count == 0 => return Err(e),
			Err(e) => {
				warn!(
					"{} failed, retrying. {} tries left. Error: {}",
					name, count, e
				);
				count -= 1;
			}
		}
	}
}
