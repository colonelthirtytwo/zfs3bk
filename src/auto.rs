use std::collections::HashMap;
use std::sync::Arc;

use futures::StreamExt;
use serde::Deserialize;

use crate::naming::RemoteBackupEntry;
use crate::naming::RemoteBackupType;
use crate::transfer::RestorePathCache;

/// Automatic snapshotting configuration
#[derive(Debug, Deserialize)]
pub struct AutoConfig {
	/// Filesystems to back up
	pub filesystems: Vec<String>,
	/// Snapshot name template. Accepts strftime-like subsititutions (ex %Y-%m-%h), will use UTC.
	pub snapshot_template: String,
	/// If specified, create a full backup if more than this many incremental backups exist. Otherwise always
	/// create incremental backups if possible.
	#[serde(default)]
	pub num_incrementals_between_fulls: Option<usize>,
	/// Optional properties to add to the snapshot.
	#[serde(default)]
	pub properties: HashMap<String, String>,
}

pub async fn auto_snapshot(
	store: Arc<crate::store::Store>,
	config: &crate::Config,
	auto_config: &AutoConfig,
) -> Result<(), String> {
	let time = chrono::Utc::now();
	let snapshot_name = &time.format(&auto_config.snapshot_template).to_string();

	let mut ok = true;

	let snapshotted = futures::stream::iter(auto_config.filesystems.iter())
		.filter(move |fs| {
			let fs = *fs;
			async move {
				info!("Creating snapshot {}@{}", fs, snapshot_name);
				let res =
					crate::zfs::create_snapshot(fs, &snapshot_name, &auto_config.properties).await;
				if let Err(err) = res {
					error!(
						"Could not create snapshot {}@{}: {}",
						fs, snapshot_name, err
					);
					false
				} else {
					true
				}
			}
		})
		.collect::<Vec<_>>()
		.await;

	for fs in snapshotted.iter() {
		if let Err(e) = upload_one(&store, config, auto_config, fs, &snapshot_name).await {
			error!(
				"Upload snapshot {}@{} failed, removing it. Error: {}",
				fs, snapshot_name, e
			);
			ok = false;

			if let Err(e) = crate::zfs::remove_snapshot(fs, &snapshot_name).await {
				error!(
					"Could not remove snapshot {}@{}, ignoring. Error: {}",
					fs, snapshot_name, e
				);
			}
		}
	}

	if ok {
		Ok(())
	} else {
		Err("One or more snapshots failed to upload".into())
	}
}

async fn upload_one(
	store: &Arc<crate::store::Store>,
	config: &crate::Config,
	auto_config: &AutoConfig,
	fs: &str,
	snapshot_name: &str,
) -> Result<(), String> {
	let dest_entry_type =
		match incremental_base(store, config, auto_config, fs, snapshot_name).await? {
			Some(v) => RemoteBackupType::Partial {
				start: v,
				end: snapshot_name.into(),
			},
			None => RemoteBackupType::Full(snapshot_name.into()),
		};

	let dest_entry = RemoteBackupEntry {
		hostname: config.hostname.clone(),
		filesystem: fs.into(),
		typ: dest_entry_type,
	};

	info!("Uploading {}@{}", fs, snapshot_name);

	crate::transfer::upload(config, store.clone(), dest_entry).await?;

	Ok(())
}

async fn incremental_base(
	store: &Arc<crate::store::Store>,
	config: &crate::Config,
	auto_config: &AutoConfig,
	fs: &str,
	snapshot_name: &str,
) -> Result<Option<String>, String> {
	let local_snapshots = crate::zfs::list_fs_snapshots(fs).await?;
	let remote_snapshots =
		crate::transfer::list_remotes(&store, config, Some(&config.hostname), Some(&fs)).await?;

	let to_pos = local_snapshots
		.iter()
		.position(|s| *s == snapshot_name)
		.unwrap();
	let most_recent = match local_snapshots[0..to_pos]
		.iter()
		.rev()
		.find(|sn| remote_snapshots.iter().any(|e| e.typ.end() == *sn))
	{
		Some(v) => v,
		None => {
			info!("No backups available locally and remotely, creating full backup");
			return Ok(None);
		}
	};

	let entries =
		crate::transfer::list_remotes(store, config, Some(&config.hostname), Some(&fs)).await?;
	let path = RestorePathCache::new(&entries).path_to(&config.hostname, &fs, &snapshot_name);
	let path = match path {
		Some(v) => v,
		None => {
			info!("No path to most recent snapshot, creating full backup");
			return Ok(None);
		}
	};
	if let Some(num) = auto_config.num_incrementals_between_fulls {
		if path.len() > num {
			info!(
				"Have {:?} backups in chain, creating full backup",
				path.len()
			);
			return Ok(None);
		}
	}

	info!(
		"Creating incremental backup starting at snapshot {:?}",
		most_recent
	);
	Ok(Some(most_recent.clone()))
}
