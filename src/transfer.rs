//! Coordinates uploading and donwloading

use std::collections::HashMap;
use std::process::Stdio;
use std::sync::Arc;

use tokio::fs::File;
use tokio::io::AsyncReadExt;

use crate::naming::RemoteBackupEntry;
use crate::naming::RemoteBackupType;
use crate::procutil::start_proc_with_cancellation;
use crate::procutil::ConfirmAsyncReader;
use crate::procutil::NamedFuture;

/// Helper for canceling.
///
/// Creates and dolls out oneshot receivers that are set if/when the task should be canceled (error or ctrl+c).
/// Fires them all when requested.
pub struct MultiCancel {
	senders: Vec<tokio::sync::oneshot::Sender<()>>,
}
impl MultiCancel {
	pub fn new() -> Self {
		Self { senders: vec![] }
	}

	pub fn subscribe(&mut self) -> tokio::sync::oneshot::Receiver<()> {
		let (send, recv) = tokio::sync::oneshot::channel();
		self.senders.push(send);
		recv
	}

	pub fn fire(self) {
		for s in self.senders.into_iter() {
			let _ = s.send(());
		}
	}
}

/// Uploads a ZFS snapshot to the store.
pub async fn upload(
	config: &crate::Config,
	store: Arc<crate::store::Store>,
	entry: RemoteBackupEntry,
) -> Result<(), String> {
	let (zstd_recv, zfs_send) = crate::procutil::pipe::<Stdio, Stdio>()
		.map_err(|e| format!("Could not create pipe: {}", e))?;
	let (gpg_recv, zstd_send) = crate::procutil::pipe::<Stdio, Stdio>()
		.map_err(|e| format!("Could not create pipe: {}", e))?;
	let (s3_recv, gpg_send) = crate::procutil::pipe::<File, Stdio>()
		.map_err(|e| format!("Could not create pipe: {}", e))?;

	let mut zfs_cmd = tokio::process::Command::new("zfs");
	zfs_cmd
		.arg("send")
		.arg("--embed")
		.arg("--props")
		.arg("--large-block")
		.kill_on_drop(true)
		.stdin(Stdio::null())
		.stdout(zfs_send)
		.stderr(Stdio::inherit());
	match entry.typ {
		RemoteBackupType::Full(ref sn) => zfs_cmd
			.arg("--")
			.arg(format!("{}@{}", entry.filesystem, sn)),
		RemoteBackupType::Partial { ref start, ref end } => zfs_cmd
			.arg("-i")
			.arg(format!("{}@{}", entry.filesystem, start))
			.arg("--")
			.arg(format!("{}@{}", entry.filesystem, end)),
	};

	let mut zstd_cmd = tokio::process::Command::new("zstd");
	zstd_cmd
		.arg("-zc")
		.arg("-T0")
		.arg(format!("-{}", config.zstd_compression_level))
		.arg("-")
		.kill_on_drop(true)
		.stdin(zstd_recv)
		.stdout(zstd_send)
		.stderr(Stdio::inherit());

	let mut gpg_cmd = if let Some(ref user) = config.gpg_download_user {
		let mut cmd = tokio::process::Command::new("sudo");
		cmd.arg("--preserve-env=GPG_TTY")
			.arg("-u")
			.arg(user)
			.arg("gpg");
		cmd
	} else {
		tokio::process::Command::new("gpg")
	};
	gpg_cmd
		.arg("--batch")
		.arg("--compress-level")
		.arg("0")
		.arg("--encrypt")
		.arg("--sign")
		.arg("--recipient")
		.arg(&config.gpg_receiver_key)
		.arg("--quiet");
	if let Some(ref sender) = config.gpg_sender_key {
		gpg_cmd.arg("--local-user").arg(sender);
	}
	if let Err(std::env::VarError::NotPresent) = std::env::var("GPG_TTY") {
		if let Ok(tty) = crate::procutil::tty().await {
			gpg_cmd.env("GPG_TTY", tty.trim());
		}
	}
	gpg_cmd
		.arg("--")
		.arg("-")
		.kill_on_drop(true)
		.stdin(gpg_recv)
		.stdout(gpg_send)
		.stderr(Stdio::inherit());

	let mut cancel = MultiCancel::new();

	let mut sig_intr = tokio::signal::unix::signal(tokio::signal::unix::SignalKind::interrupt())
		.map_err(|e| format!("Could not set up signal handler: {}", e))?;
	let mut sig_term = tokio::signal::unix::signal(tokio::signal::unix::SignalKind::terminate())
		.map_err(|e| format!("Could not set up signal handler: {}", e))?;

	let (s3_waiter, s3_confirm) = ConfirmAsyncReader::new();
	let mut s3_confirm = Some(s3_confirm);

	let task_zfs = NamedFuture::new(
		Task::Process("ZFS"),
		tokio::task::spawn(start_proc_with_cancellation(
			"zfs",
			cancel.subscribe(),
			zfs_cmd,
		)),
	);
	let task_zstd = NamedFuture::new(
		Task::Process("ZSTD"),
		tokio::task::spawn(start_proc_with_cancellation(
			"zstd",
			cancel.subscribe(),
			zstd_cmd,
		)),
	);
	let task_gpg = NamedFuture::new(
		Task::Process("GPG"),
		tokio::task::spawn(start_proc_with_cancellation(
			"gpg",
			cancel.subscribe(),
			gpg_cmd,
		)),
	);
	let task_s3 = NamedFuture::new(
		Task::Transfer,
		tokio::task::spawn({
			let key = entry.format_with(&config.backup_name_spec);
			let storage_class = config.storage_class.clone();
			let cancel = cancel.subscribe();
			async move {
				store
					.upload(
						&key,
						storage_class.as_deref(),
						cancel,
						s3_recv.chain(s3_waiter),
					)
					.await
			}
		}),
	);

	let mut cancel = Some(cancel);
	let mut select_all = futures::future::select_all(vec![task_zfs, task_zstd, task_gpg, task_s3]);
	let mut ok = true;
	loop {
		let ((name, res), remaining) = tokio::select! {
			_ = sig_intr.recv() => {
				warn!("Caught SIGINTR, canceling.");
				ok = false;
				cancel.take().map(MultiCancel::fire);
				continue;
			}
			_ = sig_term.recv() => {
				warn!("Caught SIGTERM, canceling.");
				ok = false;
				cancel.take().map(MultiCancel::fire);
				continue;
			}
			(res, _i, remaining) = &mut select_all => {
				(res, remaining)
			}
		};

		match res {
			Ok(Ok(())) => {
				info!("{} task finished", name.as_str());
			}
			Ok(Err(e)) => {
				error!("{} task failed: {}", name.as_str(), e);
				ok = false;
				cancel.take().map(MultiCancel::fire);
			}
			Err(e) => {
				error!("{} task paniced: {}", name.as_str(), e);
				ok = false;
				cancel.take().map(MultiCancel::fire);
			}
		}

		let has_processes = remaining
			.iter()
			.any(|v| matches!(v.name(), Task::Process(_)));
		if matches!(name, Task::Transfer) && has_processes && ok {
			error!("S3 Transfer task exited but previous pipeline steps haven't finished. This should never happen.");
			ok = false;
			cancel.take().map(MultiCancel::fire);
		}
		if !has_processes && ok {
			// Process pipeline has finished, signal transfer that it's ok to finish.
			let _ = s3_confirm.take().map(|s| s.send(()));
		}

		if remaining.is_empty() {
			break;
		} else {
			select_all = futures::future::select_all(remaining);
		}
	}

	if ok {
		Ok(())
	} else {
		Err("Upload failed".into())
	}
}

pub async fn download(
	config: &crate::Config,
	store: Arc<crate::store::Store>,
	entry: RemoteBackupEntry,
	destination: Option<&str>,
	dry_run: bool,
	rollback: bool,
) -> Result<(), String> {
	let dest_fs = destination
		.map(std::borrow::Cow::from)
		.unwrap_or_else(|| match entry.typ {
			RemoteBackupType::Full(_) => entry.filesystem.as_str().into(),
			RemoteBackupType::Partial { ref end, .. } => {
				format!("{}@{}", entry.filesystem, end).into()
			}
		});
	let (gpg_recv, s3_send) = crate::procutil::pipe::<Stdio, File>()
		.map_err(|e| format!("Could not create pipe: {}", e))?;
	let (zstd_recv, gpg_send) = crate::procutil::pipe::<Stdio, Stdio>()
		.map_err(|e| format!("Could not create pipe: {}", e))?;
	let (zfs_recv, zstd_send) = crate::procutil::pipe::<Stdio, Stdio>()
		.map_err(|e| format!("Could not create pipe: {}", e))?;

	let mut gpg_cmd = if let Some(ref user) = config.gpg_download_user {
		let mut cmd = tokio::process::Command::new("sudo");
		cmd.arg("--preserve-env=GPG_TTY")
			.arg("-u")
			.arg(user)
			.arg("gpg");
		cmd
	} else {
		tokio::process::Command::new("gpg")
	};
	if let Err(std::env::VarError::NotPresent) = std::env::var("GPG_TTY") {
		if let Ok(tty) = crate::procutil::tty().await {
			gpg_cmd.env("GPG_TTY", tty.trim());
		}
	}
	gpg_cmd
		.arg("--decrypt")
		.arg("--")
		.arg("-")
		.stdin(gpg_recv)
		.stdout(gpg_send)
		.stderr(Stdio::inherit());

	let mut zstd_cmd = tokio::process::Command::new("zstd");
	zstd_cmd
		.arg("-dc")
		.arg("-T0")
		.arg("-")
		.stdin(zstd_recv)
		.stdout(zstd_send)
		.stderr(Stdio::inherit());

	let mut zfs_cmd = tokio::process::Command::new("zfs");
	zfs_cmd.arg("receive");
	if dry_run {
		zfs_cmd.arg("-n");
	}
	if rollback {
		zfs_cmd.arg("-F");
	}
	zfs_cmd
		.arg("--")
		.arg(&*dest_fs)
		.kill_on_drop(true)
		.stdin(zfs_recv)
		.stdout(Stdio::inherit())
		.stderr(Stdio::inherit());

	let mut cancel = MultiCancel::new();

	let mut sig_intr = tokio::signal::unix::signal(tokio::signal::unix::SignalKind::interrupt())
		.map_err(|e| format!("Could not set up signal handler: {}", e))?;
	let mut sig_term = tokio::signal::unix::signal(tokio::signal::unix::SignalKind::terminate())
		.map_err(|e| format!("Could not set up signal handler: {}", e))?;

	let task_gpg = NamedFuture::new(
		Task::Process("GPG"),
		tokio::task::spawn(start_proc_with_cancellation(
			"gpg",
			cancel.subscribe(),
			gpg_cmd,
		)),
	);
	let task_zstd = NamedFuture::new(
		Task::Process("ZSTD"),
		tokio::task::spawn(start_proc_with_cancellation(
			"zstd",
			cancel.subscribe(),
			zstd_cmd,
		)),
	);
	let task_zfs = NamedFuture::new(
		Task::Process("ZFS"),
		tokio::task::spawn(start_proc_with_cancellation(
			"zfs",
			cancel.subscribe(),
			zfs_cmd,
		)),
	);
	let task_s3 = NamedFuture::new(
		Task::Transfer,
		tokio::task::spawn({
			let key = entry.format_with(&config.backup_name_spec);
			let cancel = cancel.subscribe();
			async move { store.download(&key, cancel, s3_send).await }
		}),
	);

	let mut cancel = Some(cancel);
	let mut select_all = futures::future::select_all(vec![task_zfs, task_zstd, task_gpg, task_s3]);
	let mut ok = true;
	loop {
		let ((name, res), remaining) = tokio::select! {
			_ = sig_intr.recv() => {
				warn!("Caught SIGINTR, canceling.");
				ok = false;
				cancel.take().map(MultiCancel::fire);
				continue;
			}
			_ = sig_term.recv() => {
				warn!("Caught SIGTERM, canceling.");
				ok = false;
				cancel.take().map(MultiCancel::fire);
				continue;
			}
			(res, _i, remaining) = &mut select_all => {
				(res, remaining)
			}
		};

		match res {
			Ok(Ok(())) => {
				info!("{} task finished", name.as_str());
			}
			Ok(Err(e)) => {
				error!("{} task failed: {}", name.as_str(), e);
				ok = false;
				cancel.take().map(MultiCancel::fire);
			}
			Err(e) => {
				error!("{} task paniced: {}", name.as_str(), e);
				ok = false;
				cancel.take().map(MultiCancel::fire);
			}
		}

		if remaining.is_empty() {
			break;
		} else {
			select_all = futures::future::select_all(remaining);
		}
	}

	if ok {
		Ok(())
	} else {
		if !dry_run {
			info!("Download failed, removing snapshot");
			match tokio::process::Command::new("zfs")
				.arg("destroy")
				.arg("--")
				.arg(&*dest_fs)
				.status()
				.await
			{
				Ok(status) if status.success() => {}
				Ok(status) => {
					warn!(
						"Could not delete snapshot, ignoring. zfs exited with status {}",
						status
					)
				}
				Err(e) => {
					warn!("Could not delete snapshot, ignoring. Error: {}", e);
				}
			};
		}

		Err("Download failed".into())
	}
}

/// List remote backups, optionally filtering by hostname and filesystem.
pub async fn list_remotes(
	store: &crate::store::Store,
	config: &crate::Config,
	hostname: Option<&str>,
	filesystem: Option<&str>,
) -> Result<Vec<RemoteBackupEntry>, String> {
	let prefix = config
		.backup_name_spec
		.prefix(hostname.as_deref(), filesystem.as_deref());
	let list = store.list_all_objects(Some(prefix.as_str())).await?;
	let mut entries = list
		.into_iter()
		.filter_map(|k| config.backup_name_spec.parse(&k).ok())
		.filter(|entry| {
			hostname.map(|hs| entry.hostname == hs).unwrap_or(true)
				&& filesystem
					.as_ref()
					.map(|fs| &entry.filesystem == fs)
					.unwrap_or(true)
		})
		.collect::<Vec<_>>();
	entries.sort_unstable_by(|a, b| a.typ.cmp(&b.typ));
	Ok(entries)
}

/*pub async fn restore_path(
	store: &crate::store::Store,
	config: &crate::Config,
	hostname: &str,
	filesystem: &str,
	snapshot: &str,
) -> Result<Option<Vec<RemoteBackupEntry>>, String> {
	let entries = list_remotes(store, config, Some(hostname), Some(filesystem)).await?;
	let cache = RestorePathCache::new(&entries);
	Ok(cache.path_to(snapshot).map(|vec| vec.into_iter().cloned().collect()))
}*/

pub struct RestorePathCache<'a> {
	entries_by_end: HashMap<(&'a str, &'a str, &'a str), Vec<&'a RemoteBackupEntry>>,
}
impl<'a> RestorePathCache<'a> {
	pub fn new(entries: &'a [RemoteBackupEntry]) -> Self {
		let mut entries_by_end =
			HashMap::<(&'a str, &'a str, &'a str), Vec<&'a RemoteBackupEntry>>::new();
		for entry in entries.iter() {
			entries_by_end
				.entry((&entry.hostname, &entry.filesystem, entry.typ.end()))
				.or_default()
				.push(entry);
		}
		Self { entries_by_end }
	}

	pub fn path_to(&self, host: &str, fs: &str, end: &str) -> Option<Vec<&'a RemoteBackupEntry>> {
		let entries = self
			.entries_by_end
			.get(&(host, fs, end))
			.map(Vec::as_slice)
			.unwrap_or_default();
		let mut min_path: Option<Vec<&RemoteBackupEntry>> = None;
		for entry in entries {
			let path = match entry.typ {
				RemoteBackupType::Full(_) => Some(vec![]),
				RemoteBackupType::Partial { ref start, .. } => self.path_to(host, fs, &start),
			};
			if let Some(mut path) = path {
				path.push(*entry);
				if min_path
					.as_ref()
					.map(|mp| path.len() < mp.len())
					.unwrap_or(true)
				{
					min_path = Some(path)
				}
			}
		}
		min_path
	}
}

#[derive(Debug, Clone)]
enum Task {
	Process(&'static str),
	Transfer,
	//Signal(&'static str),
}
impl Task {
	fn as_str(&self) -> &str {
		match self {
			Self::Process(v) => v,
			Self::Transfer => "S3 Transfer",
			//Self::Signal(v) => v,
		}
	}
}
