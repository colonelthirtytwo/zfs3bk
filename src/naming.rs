//! Parses the backup naming conventions.

use std::fmt::Display;
use std::fmt::Write as _;
use std::fmt::{self,};
use std::ops::Range;

use regex::Regex;

#[derive(Debug)]
pub struct BackupNameSpec {
	base: String,
	index_hostname: usize,
	index_filesystem: usize,
	index_snapshot: usize,
	regex: Regex,
}
impl BackupNameSpec {
	pub fn new(spec: &str) -> Result<Self, String> {
		let span_hostname = Self::find_one("{hostname}", &spec)?;
		let span_filesystem = Self::find_one("{filesystem}", &spec)?;
		let span_snapshot = Self::find_one("{snapshot}", &spec)?;

		let mut index_hostname = usize::MAX;
		let mut index_filesystem = usize::MAX;
		let mut index_snapshot = usize::MAX;

		let mut ordered_ranges = [
			(
				span_hostname,
				&mut index_hostname,
				r"(?P<hostname>[a-zA-Z0-9 \t\-_,.:]+)",
			),
			(
				span_filesystem,
				&mut index_filesystem,
				r"(?P<filesystem>[a-zA-Z0-9 \t\-_,.:/]+)",
			),
			(
				span_snapshot,
				&mut index_snapshot,
				r"(?P<snapshot>[a-zA-Z0-9 \t\-_,.:]+(?:\|[a-zA-Z0-9 \t\-_,.:]+)?)",
			),
		];
		ordered_ranges.sort_unstable_by_key(|v| v.0.start);

		let mut base = String::new();
		let mut regex_spec = String::from("^");

		let mut range_offset = 0;
		let mut last_end = 0;
		for (range, index_dest, capture) in ordered_ranges.iter_mut() {
			**index_dest = range.start - range_offset;

			base.push_str(&spec[last_end..range.start]);
			regex_spec.push_str(&regex::escape(&spec[last_end..range.start]));
			regex_spec.push_str(*capture);

			last_end = range.end;
			range_offset += range.end - range.start;
		}

		base.push_str(&spec[last_end..]);
		regex_spec.push_str(&regex::escape(&spec[last_end..]));
		regex_spec.push('$');

		debug!("Regex for spec {:?}: {:?}", spec, regex_spec);
		let regex = regex::Regex::new(&regex_spec).unwrap();

		Ok(Self {
			base,
			index_hostname,
			index_filesystem,
			index_snapshot,
			regex,
		})
	}

	fn find_one(needle: &str, haystack: &str) -> Result<Range<usize>, String> {
		let range = match haystack.find(needle) {
			Some(i) => i..i + needle.len(),
			None => return Err(format!("Missing {:?} in backup name specification", needle)),
		};
		let remaining = &haystack[range.end..];
		if remaining.find(needle).is_some() {
			return Err(format!(
				"Specifier {:?} appears in backup name specification multiple times",
				needle
			));
		}
		Ok(range)
	}

	pub fn format(&self, hostname: &str, filesystem: &str, backup: &RemoteBackupType) -> String {
		let mut indices_in_order = [
			(self.index_hostname, &hostname as &dyn Display),
			(self.index_filesystem, &filesystem as &dyn Display),
			(self.index_snapshot, backup as &dyn Display),
		];
		indices_in_order.sort_unstable_by_key(|v| v.0);

		let mut last_index = 0;
		let mut str = String::with_capacity(
			self.base.len() + hostname.len() + filesystem.len() + backup.strlen(),
		);
		for (index, display) in indices_in_order {
			str.push_str(&self.base[last_index..index]);
			write!(&mut str, "{}", display).unwrap();
			last_index = index;
		}
		str.push_str(&self.base[last_index..]);

		str
	}

	pub fn prefix(&self, hostname: Option<&str>, filesystem: Option<&str>) -> String {
		let mut indices_in_order = [
			(self.index_hostname, hostname),
			(self.index_filesystem, filesystem),
			(self.index_snapshot, None),
		];
		indices_in_order.sort_unstable_by_key(|v| v.0);

		let mut last_index = 0;
		let mut str = String::with_capacity(indices_in_order[0].0);
		for (index, display) in indices_in_order {
			str.push_str(&self.base[last_index..index]);
			if let Some(display) = display {
				str.push_str(display);
				last_index = index;
			} else {
				break;
			}
		}

		str
	}

	pub fn parse(&self, str: &str) -> Result<RemoteBackupEntry, ()> {
		let re_match = self.regex.captures(str).ok_or(())?;
		let hostname = re_match.name("hostname").unwrap().as_str().into();
		let filesystem = re_match.name("filesystem").unwrap().as_str().into();
		let snapshot_t = re_match.name("snapshot").unwrap().as_str();
		let snapshot = if let Some((a, b)) = snapshot_t.split_once('|') {
			RemoteBackupType::Partial {
				start: a.into(),
				end: b.into(),
			}
		} else {
			RemoteBackupType::Full(snapshot_t.into())
		};

		Ok(RemoteBackupEntry {
			hostname,
			filesystem,
			typ: snapshot,
		})
	}
}
impl<'de> serde::Deserialize<'de> for BackupNameSpec {
	fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
	where
		D: serde::Deserializer<'de>,
	{
		use serde::de::Error as _;
		let s = <&'de str>::deserialize(deserializer)?;
		Self::new(s).map_err(|e| D::Error::custom(e))
	}
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, PartialOrd)]
pub enum RemoteBackupType {
	Full(String),
	Partial { start: String, end: String },
}
impl RemoteBackupType {
	pub fn start(&self) -> &str {
		match self {
			Self::Full(v) => v,
			Self::Partial { start, .. } => start,
		}
	}

	pub fn end(&self) -> &str {
		match self {
			Self::Full(v) => v,
			Self::Partial { end, .. } => end,
		}
	}

	fn strlen(&self) -> usize {
		match self {
			Self::Full(v) => v.len(),
			Self::Partial { start, end } => start.len() + end.len() + 1,
		}
	}
}
impl fmt::Display for RemoteBackupType {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		match self {
			Self::Full(v) => v.fmt(f),
			Self::Partial { start, end } => write!(f, "{}|{}", start, end),
		}
	}
}
impl Ord for RemoteBackupType {
	fn cmp(&self, other: &Self) -> std::cmp::Ordering {
		self.start()
			.cmp(other.start())
			.then(self.end().cmp(other.end()))
	}
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct RemoteBackupEntry {
	pub hostname: String,
	pub filesystem: String,
	pub typ: RemoteBackupType,
}
impl RemoteBackupEntry {
	pub fn format_with(&self, spec: &BackupNameSpec) -> String {
		spec.format(&self.hostname, &self.filesystem, &self.typ)
	}

	pub fn parse_with(spec: &BackupNameSpec, str: &str) -> Result<Self, ()> {
		spec.parse(str)
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test_spec_usual() {
		let spec = BackupNameSpec::new("backup|{hostname}|{filesystem}|{snapshot}").unwrap();
		assert_eq!(
			spec.format(
				"nas.lan",
				"nas/syncthing",
				&RemoteBackupType::Full("backup-1".into())
			),
			"backup|nas.lan|nas/syncthing|backup-1"
		);
		assert_eq!(
			spec.format(
				"nas.lan",
				"nas/syncthing",
				&RemoteBackupType::Partial {
					start: "backup-1".into(),
					end: "backup-2".into()
				}
			),
			"backup|nas.lan|nas/syncthing|backup-1|backup-2"
		);

		assert_eq!(
			spec.prefix(Some("nas.lan"), Some("nas/syncthing")),
			"backup|nas.lan|nas/syncthing|"
		);

		assert_eq!(spec.prefix(None, None,), "backup|");

		assert_eq!(spec.prefix(None, Some("nas/syncthing"),), "backup|");

		let entry1 =
			RemoteBackupEntry::parse_with(&spec, "backup|nas.lan|nas/syncthing|backup-1").unwrap();
		assert_eq!(entry1.hostname, "nas.lan");
		assert_eq!(entry1.filesystem, "nas/syncthing");
		assert_eq!(entry1.typ, RemoteBackupType::Full("backup-1".into()));

		let entry2 =
			RemoteBackupEntry::parse_with(&spec, "backup|nas.lan|nas/syncthing|backup-1|backup-2")
				.unwrap();
		assert_eq!(entry2.hostname, "nas.lan");
		assert_eq!(entry2.filesystem, "nas/syncthing");
		assert_eq!(
			entry2.typ,
			RemoteBackupType::Partial {
				start: "backup-1".into(),
				end: "backup-2".into()
			}
		);

		assert!(RemoteBackupEntry::parse_with(&spec, "hello world i am not a match").is_err());
	}

	#[test]
	fn test_spec_out_of_order() {
		let spec = BackupNameSpec::new("backup|{filesystem}|{hostname}|{snapshot}").unwrap();
		assert_eq!(
			spec.format(
				"nas.lan",
				"nas/syncthing",
				&RemoteBackupType::Full("backup-1".into())
			),
			"backup|nas/syncthing|nas.lan|backup-1"
		);
		assert_eq!(
			spec.format(
				"nas.lan",
				"nas/syncthing",
				&RemoteBackupType::Partial {
					start: "backup-1".into(),
					end: "backup-2".into()
				}
			),
			"backup|nas/syncthing|nas.lan|backup-1|backup-2"
		);

		assert_eq!(
			spec.prefix(Some("nas.lan"), Some("nas/syncthing")),
			"backup|nas/syncthing|nas.lan|"
		);

		assert_eq!(spec.prefix(None, None,), "backup|");

		assert_eq!(
			spec.prefix(None, Some("nas/syncthing"),),
			"backup|nas/syncthing|"
		);

		let entry1 =
			RemoteBackupEntry::parse_with(&spec, "backup|nas/syncthing|nas.lan|backup-1").unwrap();
		assert_eq!(entry1.hostname, "nas.lan");
		assert_eq!(entry1.filesystem, "nas/syncthing");
		assert_eq!(entry1.typ, RemoteBackupType::Full("backup-1".into()));

		let entry2 =
			RemoteBackupEntry::parse_with(&spec, "backup|nas/syncthing|nas.lan|backup-1|backup-2")
				.unwrap();
		assert_eq!(entry2.hostname, "nas.lan");
		assert_eq!(entry2.filesystem, "nas/syncthing");
		assert_eq!(
			entry2.typ,
			RemoteBackupType::Partial {
				start: "backup-1".into(),
				end: "backup-2".into()
			}
		);

		assert!(RemoteBackupEntry::parse_with(&spec, "hello world i am not a match").is_err());
	}

	#[test]
	fn test_spec_parse_fail() {
		assert!(BackupNameSpec::new("backup|").is_err());
		assert!(BackupNameSpec::new("backup|{hostname}").is_err());
		assert!(BackupNameSpec::new("backup|{hostname}|{filesystem}").is_err());
		assert!(BackupNameSpec::new("backup|{hostname}|{hostname}|{snapshot}").is_err());
		assert!(
			BackupNameSpec::new("backup|{hostname}|{hostname}|{filesystem}|{snapshot}").is_err()
		);
	}
}
