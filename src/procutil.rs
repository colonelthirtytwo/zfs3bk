use std::future::Future;
use std::os::unix::prelude::FromRawFd;
use std::task::Poll;

use nix::fcntl::OFlag;
use pin_project::pin_project;

/// Spawns and monitors a process.
///
/// If the cancel oneshsot fires, kills the process.
///
/// Returns an error if the process didn't exit successfully.
pub async fn start_proc_with_cancellation(
	name: &str,
	mut cancel: tokio::sync::oneshot::Receiver<()>,
	mut proc: tokio::process::Command,
) -> Result<(), String> {
	debug!("Executing: {:?}", proc);
	let mut proc = proc
		.spawn()
		.map_err(|e| format!("Could not execute process {}: {}", name, e))?;

	tokio::select! {
		_ = &mut cancel => {
			if let Some(pid) = proc.id() {
				debug!("Canceling process {} (pid {})", name, pid);
				let _ = nix::sys::signal::kill(
					nix::unistd::Pid::from_raw(pid as _),
					nix::sys::signal::Signal::SIGTERM,
				);
				let _ = proc.wait().await;
			}
			Err(format!("Process {} canceled", name))
		},
		res = proc.wait() => {
			let res = res.map_err(|e| format!("Waiting on process {} failed: {}", name, e))?;
			if res.success() {
				Ok(())
			} else {
				Err(format!("Process {} exited with code {}", name, res))
			}
		}
	}
}

/// Creates a POSIX pipe, wrapped in any FD wrapper type.
pub fn pipe<R: FromRawFd, W: FromRawFd>() -> Result<(R, W), nix::Error> {
	unsafe {
		let raw_fds = nix::unistd::pipe2(OFlag::O_CLOEXEC)?;
		Ok((R::from_raw_fd(raw_fds.0), W::from_raw_fd(raw_fds.1)))
	}
}

/// Gets the tty, by running the `tty` command.
pub async fn tty() -> Result<String, String> {
	let out = tokio::process::Command::new("tty")
		.stderr(std::process::Stdio::inherit())
		.output()
		.await
		.map_err(|e| format!("Could not execute `tty`: {}", e))?;
	if !out.status.success() {
		return Err(format!("`tty` exited with code {}", out.status));
	}
	String::from_utf8(out.stdout).map_err(|e| format!("tty path is not utf-8: {}", e))
}

/// Future with some associated data.
///
/// For identification with select_all
#[pin_project]
pub struct NamedFuture<N, F> {
	name: N,
	#[pin]
	fut: F,
}
impl<N, F> NamedFuture<N, F> {
	pub fn new(name: N, fut: F) -> Self {
		Self { name, fut }
	}

	pub fn name(&self) -> &N {
		&self.name
	}
}
impl<N, F> std::ops::Deref for NamedFuture<N, F> {
	type Target = F;

	fn deref(&self) -> &Self::Target {
		&self.fut
	}
}
impl<N, F> std::ops::DerefMut for NamedFuture<N, F> {
	fn deref_mut(&mut self) -> &mut Self::Target {
		&mut self.fut
	}
}
impl<N, F> std::future::Future for NamedFuture<N, F>
where
	N: Clone,
	F: std::future::Future,
{
	type Output = (N, F::Output);

	fn poll(self: std::pin::Pin<&mut Self>, cx: &mut std::task::Context<'_>) -> Poll<Self::Output> {
		let this = self.project();
		match this.fut.poll(cx) {
			Poll::Ready(v) => Poll::Ready((this.name.clone(), v)),
			Poll::Pending => Poll::Pending,
		}
	}
}

/// AsyncRead that waits for a "confirmation" before yielding EOF. Meant to be used with chain.
pub struct ConfirmAsyncReader {
	confirm: Option<tokio::sync::oneshot::Receiver<()>>,
}
impl ConfirmAsyncReader {
	pub fn new() -> (Self, tokio::sync::oneshot::Sender<()>) {
		let (s, r) = tokio::sync::oneshot::channel();
		(Self { confirm: Some(r) }, s)
	}
}
impl tokio::io::AsyncRead for ConfirmAsyncReader {
	fn poll_read(
		mut self: std::pin::Pin<&mut Self>,
		cx: &mut std::task::Context<'_>,
		_buf: &mut tokio::io::ReadBuf<'_>,
	) -> Poll<std::io::Result<()>> {
		match self
			.confirm
			.as_mut()
			.map(|r| std::pin::Pin::new(r).poll(cx))
		{
			Some(Poll::Ready(Ok(()))) => {
				self.confirm = None;
				return Poll::Ready(Ok(()));
			}
			Some(Poll::Ready(Err(_))) => {
				panic!("confirmer dropped");
			}
			Some(Poll::Pending) => return Poll::Pending,
			None => return Poll::Ready(Ok(())),
		}
	}
}
